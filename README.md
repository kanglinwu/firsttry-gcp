# 在LocalServer建立VueCli, 然後產生static file, 再部署到Google Cloud Platform App engine
> 部署程式為Vue Cli <br>
> nodejs版本: 12<br>
> 使用的服務是Google-Cloud-Platform的App engine<br>

1. 建立GCP Account
2. 下載Google Cloud SDK
3. 到要部署的folder裡執行Google Cloud初始化
4. 先將要部署上去的打包成static files
5. 建立yaml file
6. 部署
7. 確認狀況
---
### 1 - 先建立Google Cloud Platform Account
> 開通帳號後, 開啟App engine API
### 2 - 下載SDK
> [download Google Cloud SDK page](https://cloud.google.com/sdk)
### 3 - 初始化指令
<code>gcloud init</code>
> 需要選擇google account/project name/config name/COMPUTE_DEFAULT_ZONE/COMPUTE_DEFAULT_REGION
### 4 - vue打包
<code>npm run build</code><br>
> 打包完會在目前要部署上去的資料夾裡自動新增一個dist的folder

### 5 - 設定yaml file
> name - app.yaml <br>

```yaml
runtime: nodejs12
handlers:
  # Serve all static files with urls ending with a file extension
  - url: /(.*\..+)$
    static_files: dist/\1
    upload: dist/(.*\..+)$
    # catch all handler to index.html
  - url: /.*
    static_files: dist/index.html
    upload: dist/index.html
```
### 6 - 部署
<code>gcloud app deploy</code><br>

### 7 - 到GCP Console確認狀況
![在App engine裡點開url](https://i.ibb.co/pQQvr3c/Snipaste-2021-08-02-05-17-14.png)

##### Others - 停止App
![停止](https://i.ibb.co/jy7vpSZ/Snipaste-2021-08-02-05-19-01.png)

